import React, { Component } from 'react';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';

import Aux from '../../hoc/Auxiliary/Auxiliary';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummery from '../../components/Burger/OrderSummery/OrderSummery';
import axios from '../../axios-orders';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import { connect } from 'react-redux';
import * as actionType from '../../store/actions';

class BurgerBuilder extends Component {
    state = {
        // ingredients: null,
        // totalPrice: 4,
        purchasable: false,
        purchasing: false,
        loading: false,
        error: false
    }

    componentDidMount() {
       
        // axios.get('https://react-my-burger-bbd40-default-rtdb.firebaseio.com/ingredients.json')
        //     .then(response => {
        //         this.setState({
        //             ingredients: response.data
        //         })
        //     })
        //     .catch(error => {
        //         this.setState({
        //             error: true
        //         })
        //     });
    }

    purchaseHandler = () => {
        this.setState({
            purchasing: true
        })
    }

    purchaseCancelHandler = () => {
        this.setState({
            purchasing: false
        })
    }

    purchaseContinueHandler = () => {
        // /*
        

        // */ 
        // const queryParams = [];
        // for (let i in this.state.ingredients) {
        //     queryParams.push(encodeURIComponent(i) + '=' + encodeURIComponent(this.state.ingredients[i]));
        // }
        // queryParams.push('price=' + this.state.price);

        // const queryString = queryParams.join('&');
        this.props.history.push('/checkout');   
    }

    updatePurchasableState = (ingredients) => {

        const sum = Object.keys(ingredients)
            .map(igKey => {
                return ingredients[igKey];
            })
            .reduce((sum, el) => {
                return sum + el;
            }, 0);

        // this.setState({
        //     purchasable: sum > 0
        // })
        return sum > 0;
    }

    // addIngrdientHandler = (type) => {
    //     const oldCount = this.state.ingredients[type];
    //     const updatedCount = oldCount + 1;
    //     const updateIngredient = {
    //         ...this.state.ingredients
    //     };

    //     updateIngredient[type] = updatedCount;
    //     const priceAddition = INGREDIENT_PRICES[type];
    //     const oldPrice = this.state.totalPrice;
    //     const newPrice = oldPrice + priceAddition;
    //     this.setState({
    //         ingredients: updateIngredient,
    //         totalPrice: newPrice
    //     })
    //     this.updatePurchasableState(updateIngredient);
    // }

    // removeIngrdientHandler = (type) => {
    //     const oldCount = this.state.ingredients[type];
    //     if (oldCount <= 0) {
    //         return;
    //     }
    //     const updatedCount = oldCount - 1;
    //     const updateIngredient = {
    //         ...this.state.ingredients
    //     };

    //     updateIngredient[type] = updatedCount;
    //     const priceDeduction = INGREDIENT_PRICES[type];
    //     const oldPrice = this.state.totalPrice;
    //     const newPrice = oldPrice - priceDeduction;
    //     this.setState({
    //         ingredients: updateIngredient,
    //         totalPrice: newPrice
    //     })
    //     this.updatePurchasableState(updateIngredient);
    // }

    render() {
        const disabledInfo = {
            // ...this.state.ingredients
            ...this.props.ings
        };

        for (let key in disabledInfo) {
            disabledInfo[key] = disabledInfo[key] <= 0
        }

        let orderSummery = null;

        if (this.state.loading) {
            orderSummery = <Spinner />;
        }

        let burger = this.state.error ? <p>Error Occur</p> : <Spinner />;

        // if (this.state.ingredients) {
        if (this.props.ings) {    

            orderSummery = <OrderSummery
                ingredients={this.props.ings}
                price={this.props.price.toFixed(2)}
                purchaseCancelled={this.purchaseCancelHandler}
                purchaseContinued={this.purchaseContinueHandler} />;

            burger = (
                <Aux>
                    <Burger ingredients={this.props.ings} />
                    <BuildControls
                        // ingredientAdded={this.addIngrdientHandler}
                        // ingredientRemoved={this.removeIngrdientHandler}
                        ingredientAdded={this.props.onIngredientAdded}
                        ingredientRemoved={this.props.onIngredientRemoved}
                        disabled={disabledInfo}
                        purchasable={this.updatePurchasableState(this.props.ings)}
                        price={this.props.price}
                        ordered={this.purchaseHandler} />
                </Aux>
            );
        }


        return (
            <Aux>
                <Modal
                    show={this.state.purchasing}
                    modelClosed={this.purchaseCancelHandler}>
                    {orderSummery}
                </Modal>
                {burger}
            </Aux>

        )
    }
}


const mapStateToProps = state => {
    return {
        ings: state.ingredients,
        price: state.totalPrice
    }
}

const mapDispatchToProps = dispatch => {

    const onIngredientAdded = (ingName) => dispatch({
        type: actionType.ADD_INGREDIENT,
        ingredientName: ingName
    });

    const onIngredientRemoved = (ingName) => dispatch({
        type: actionType.REMOVE_INGREDIENT,
        ingredientName: ingName
    });

    return {
        onIngredientAdded: onIngredientAdded,
        onIngredientRemoved: onIngredientRemoved
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (withErrorHandler(BurgerBuilder, axios));