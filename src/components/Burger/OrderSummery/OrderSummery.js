import React from 'react';
import Aux from '../../../hoc/Auxiliary/Auxiliary';
import Button from '../../UI/Button/Button';

const orderSummery = (props) => {

    const ingredientSummery = Object.keys(props.ingredients)
        .map(igKey => {
            return <li>
                <span style={{textTransform: 'capitalize'}}>{igKey}</span> : {props.ingredients[igKey]}
            </li>
        });
    console.log("Order summery update");    
    return (
        <Aux>
            <h3>Your Order</h3>
            <p>Dedicated Burger with following ingredients</p>
            <ul>
                {ingredientSummery}
            </ul>
            <p><strong>Total Price: {props.price}</strong></p>
            <p>Continue to checkout?</p>
            <Button btnType="Danger" clicked={props.purchaseCancelled}>CANCEL</Button>
            <Button btnType="Success" clicked={props.purchaseContinued}>CONTINUE</Button>
        </Aux>
    )
}

export default orderSummery;