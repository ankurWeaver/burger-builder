import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://react-my-burger-bbd40-default-rtdb.firebaseio.com/'
})

export default instance;